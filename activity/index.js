document.querySelector("txt-first-name");//selector for JS;this is a function; string in () is a tag, class or id
document.querySelector("txt-last-name");
/*
document - refers to whole webpage
querySelector - used to select a specific element(object) as long as its inside the html tag (HTML element)
	-takes a string input that is formatted like CSS selector
	-can select elements regardless if the string is an id, class or tag; as long as element is existing in the webpage


document.querySelector is similar to these 3:

document.getElementById("txt-first-name");
document.getElementByClass("txt-inputs");
document.getElementByTagName("input");
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtFullName = document.querySelector("#span-full-name");

var first = txtFirstName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML = txtFirstName.value
})
/*
event - actoins that user is doing in our webpage (scroll, click, hover, keypress/type)

addEventListener - a function that lets the webpage to listen to the events performed by the user
	-takes 2 arguments
		-string - the event on which the HTML element will listen; keyup, onclick; values are predetermined
		-function - executed by the element once the event(first argument) is triggered
*/

txtFirstName.addEventListener("keyup", (event) => {
	//event.target is logging the codes instead of its value, once event is triggered
	console.log(event.target);
	//event.target.value-logs value of element, once event is triggered
	console.log(event.target.value);
})

//MINIACTIVITY - make another "keyup" event

const txtLastName = document.querySelector("#txt-last-name");

var last = txtLastName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML = txtLastName.value;
	console.log(event.target);
	console.log(event.target.value);	
})


//ACTIVITY
//var first = document.querySelector("#txt-first-name").value;
//var last = document.querySelector("#txt-last-name").value;
/*const completeName = document.querySelector('#txt-first-name, #txt-last-name');

completeName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML = completeName.value;
	console.log(event.target);
	console.log(event.target.value);	
})
*/

txtFullName.addEventListener("keyup", (event) =>{
	txtFullName.innerHTML += txtFirstName.value
	txtFullName.innerHTML += txtLastName.value;	
	console.log(event.target);
	console.log(event.target.value);	
})
